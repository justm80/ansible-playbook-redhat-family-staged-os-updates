# Staged OS Updates

&#10071; 
`Currently only RedHat family is supported`

&#10071; 
`Each server needs to have the same OS`

&#10071; 
`Before using this, be sure to have all hosts on the same patch level`

&#10071; 
`A filter is in place where only the [base] repo is being checked for updates`



# Flow steps

When NO tags are used the playbook will

1. Check which updates are available on the 1st host avialable in your inventory list
2. Save a list of available updates in a file
3. Read the available updates list from the file
4. Run updates on all hosts avialable in your inventory list
5. Check and notify if the server needs a reboot

## tag: check_updates

This tag will only run steps (1 and 2)

## tag: run_updates

This tag will only run steps (3, 4 and 5)

## tag: check_reboot

This tag will only run step (5)


# Playbook usage

```bash
ansible-playbook -i hosts.ini staged-osupdates.yml --tags check_updates
ansible-playbook -i hosts.ini staged-osupdates.yml --tags run_updates
```

## TAP Staged example

### Test

On `your_check_for_avialable_updates_date` run the check_updates tag against your test inventory

```bash
ansible-playbook -i test_hosts.ini staged-osupdates.yml --tags check_updates
```

On `your_run_test_updates_date` run the run_updates tag against your test inventory

```bash
ansible-playbook -i test_hosts.ini staged-osupdates.yml --tags run_updates
```


### Acceptance

On `your_run_acceptance_updates_date` run the run_updates tag against your acceptance inventory

```bash
ansible-playbook -i acceptance_hosts.ini staged-osupdates.yml --tags run_updates
```


### Production

On `your_run_production_updates_date` run the run_updates tag against your production inventory

```bash
ansible-playbook -i production_hosts.ini staged-osupdates.yml --tags run_updates
```


## TAP Example 

| When                    | Enviroment  | Tags              |
|-------------------------|-------------|-------------------|
| 1st friday of the month | Test        | `check_updates`   |
| 1st friday of the month | Test        | `run_updates`     |
| 2nd friday of the month | Acceptance  | `run_updates`     |
| 3rd friday of the month | Production  | `run_updates`     |


# Variable override

The variables below can be overridden.

```yaml
staged_osupdates_work_dir:      "./staged-osupdates"
groupname:                      "all"
staged_osupdates_disablerepo:   "*"
staged_osupdates_enablerepo:    "base"
```

### var: staged_osupdates_work_dir

This variable controls where the staged update list is saved.
This is also the location where the output logfile is saved.

```
./staged-osupdates/
├── available_updates.all.hosts.packages
└── hostname.20211110T113912.yum.log

0 directories, 2 files
```

### var: groupname

This variable allows you to even further limit the hosts being used in the updates.
Mostly never used, but it is possible

Example

```bash
ansible-playbook -i hosts.ini staged-osupdates.yml --tags run_updates -e 'groupname=opentunnel'
```


### var: staged_osupdates_disablerepo

This variable can contain a comma sep list of disabled repos

### var: staged_osupdates_enablerepo

This variable can contain a comma sep list of enabled repos
